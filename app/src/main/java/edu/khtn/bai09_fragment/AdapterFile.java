package edu.khtn.bai09_fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.List;

public class AdapterFile extends BaseAdapter {
    private List<File> list;
    Context context;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.model_list_file, null, false);
        TextView textView = (TextView) view.findViewById(R.id.fileList_model);
        if (list.get(position).isFile())
            textView.setText("File: "+list.get(position).getName());
        else
            textView.setText("Dir : "+list.get(position).getName());
        return view;
    }

    public AdapterFile() {
        super();
    }

    public AdapterFile(List<File> list) {
        this.list = list;
    }

    public AdapterFile(Context context) {
        this.context = context;
    }

    public AdapterFile(List<File> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public List getList() {
        return list;
    }

    public void setList(List<File> list) {
        this.list = list;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
