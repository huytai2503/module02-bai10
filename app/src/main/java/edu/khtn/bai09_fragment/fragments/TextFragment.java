package edu.khtn.bai09_fragment.fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class TextFragment extends Fragment{
    static public String CURRENT_PATH = "";
    LinearLayout textLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = new ScrollView(getActivity());
        ScrollView scrollView = (ScrollView) view.getRootView();
        textLayout = new LinearLayout(getActivity());
        textLayout.setOrientation(LinearLayout.VERTICAL);
        textLayout.setBackgroundColor(Color.parseColor("#f0f0f0"));
        scrollView.addView(textLayout);
        File file = new File(CURRENT_PATH);
        ArrayList<String> lineList = readTextFile(file);
        for (String line : lineList) {
            TextView textView = new TextView(getActivity());
            textView.setText(line);
            textLayout.addView(textView);
        }
        return view;
    }

    public ArrayList readTextFile(File file){
        try {
            Scanner sc = new Scanner(file);
            ArrayList<String> stringList = new ArrayList<>();
            while (sc.hasNextLine())
                stringList.add(sc.nextLine());
            return stringList;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }return  null;
    }
}
