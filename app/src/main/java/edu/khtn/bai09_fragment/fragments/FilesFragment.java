package edu.khtn.bai09_fragment.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.khtn.bai09_fragment.AdapterFile;
import edu.khtn.bai09_fragment.R;

public class FilesFragment extends Fragment implements View.OnClickListener {
    static public List<File> currentFileList = new ArrayList<>();
    static public List<List<File>> arrFilesList = new ArrayList<>();
    static public String CURRENT_PATH = "";
    View view;
    LinearLayout filesLayout;
    AdapterFile adapterFile;
    ListView listView;
    int pathLevel = 0;
    String[] mediaExtenList, imageExtenList, textExtenList;
    Button btnNewDir, btnDelete, btnBack;
    TextView txtPath;
    EditText edtName;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_files, null, false);
        filesLayout = (LinearLayout) view.findViewById(R.id.filesLayout);
        findAllViewAndSetOnClick();
        currentFileList = getExternalStorageList();
        arrFilesList.add(currentFileList);
        CURRENT_PATH = currentFileList.get(0).getParent();
        txtPath.setText(CURRENT_PATH);
        adapterFile = new AdapterFile(getActivity());
        adapterFile.setList(currentFileList);
        listView = new ListView(getActivity());
        listView.setAdapter(adapterFile);
        filesLayout.addView(listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File choosenFile = currentFileList.get(position);
                if (choosenFile.isDirectory()) {
                    openDir(choosenFile);
                } else if (choosenFile.isFile()) {
                    openFile(choosenFile);
                }
            }
        });
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.newDir_btn:
                newDir();
                break;
            case R.id.delete_btn:
                delete();
                break;
            case R.id.back_btn:
                back();
                break;
            case R.id.path_text:
                break;
            case R.id.name_edit:
                break;
            default:
                break;
        }
    }

    public void back() {
        if (pathLevel > 0) {
            filesLayout.removeView(listView);
            arrFilesList.remove(pathLevel);
            pathLevel--;
            currentFileList = arrFilesList.get(pathLevel);
            adapterFile.setList(currentFileList);
            CURRENT_PATH = currentFileList.get(0).getParent();
            txtPath.setText(CURRENT_PATH);
            listView.setAdapter(adapterFile);
            filesLayout.addView(listView);
        }
    }

    public void newDir() {
        if (isStorageWritable()) {
            String folderName = edtName.getText().toString();
            File file = new File(CURRENT_PATH, folderName);
            if (folderName.isEmpty()) {
                toast("Type the name");
            }else if (file.exists()) {
                toast(folderName + " is existed");
            }else {
                file.mkdir();
                toast(folderName + " is created");
                currentFileList.add(file);
                arrFilesList.remove(pathLevel);
                arrFilesList.add(currentFileList);
                adapterFile.setList(currentFileList);
                listView.setAdapter(adapterFile);
                filesLayout.removeView(listView);
                filesLayout.addView(listView);
            }
        }
    }

    public void delete() {
        if (isStorageWritable()) {
            String folderName = edtName.getText().toString();
            File file = new File(CURRENT_PATH, folderName);
            if (folderName.isEmpty()) {
                toast("Type the name");
            }else if (file.exists()) {
                file.delete();
                currentFileList.remove(file);
                arrFilesList.get(pathLevel).remove(file);
                adapterFile.setList(currentFileList);
                listView.setAdapter(adapterFile);
                filesLayout.removeView(listView);
                filesLayout.addView(listView);
                toast(folderName + " is deleted");
            }else {
                toast(folderName + " is not existed");
            }
        }
    }

    public void openDir(File file) {
        pathLevel++;
        currentFileList = getFileList(file);
        adapterFile.setList(currentFileList);
        arrFilesList.add(currentFileList);
        if (currentFileList.isEmpty() == false)
            CURRENT_PATH = currentFileList.get(0).getParent();
        else
            CURRENT_PATH += "/" + file.getName();
        txtPath.setText(CURRENT_PATH);
        filesLayout.removeView(listView);
        listView.setAdapter(adapterFile);
        filesLayout.addView(listView);
    }

    public void openFile(File file) {
        mediaExtenList = new String[]{".mp3", ".wav", ".wma", ".3gp", ".m4a"};
        for (String s : mediaExtenList) {
            if (file.getName().endsWith(s)) {
                Mp3Fragment.CURRENT_PATH = CURRENT_PATH + "/" + file.getName();
                Fragment mp3Fragment = new Mp3Fragment();
                addFragmentToHomeLayout(mp3Fragment, "mp3", true);
                break;
            }
        }
        imageExtenList = new String[]{".jpg", ".png", ".bmp", ".gif", ".jpeg"};
        for (String s : imageExtenList) {
            if (file.getName().endsWith(s)) {
                ImgFragment.CURRENT_PATH = CURRENT_PATH + "/" + file.getName();
                Fragment imgFragment = new ImgFragment();
                addFragmentToHomeLayout(imgFragment, "image", true);
                break;
            }
        }
        textExtenList = new String[]{".txt", ".bin", ".bat", ".java"};
        for (String s : textExtenList) {
            if (file.getName().endsWith(s)) {
                TextFragment.CURRENT_PATH = CURRENT_PATH + "/" + file.getName();
                Fragment textFragment = new TextFragment();
                addFragmentToHomeLayout(textFragment, "text", true);
                break;
            }
        }
    }

    public void addFragmentToHomeLayout
            (Fragment fragment, String key, boolean addToStack) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.homeLayout, fragment);
        if (addToStack)
            transaction.addToBackStack(key);
        transaction.commit();
    }

    public List getFileList(File file) {
        List fileList = new ArrayList();
        for (File f : file.listFiles()) {
            fileList.add(f);
        }
        return fileList;
    }

    public List getExternalStorageList() {
        if (isStorageReadable()) {
            File sdcard = Environment.getExternalStorageDirectory();
            return getFileList(sdcard);
        } else
            return null;
    }

    public boolean isStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
            return true;
        return false;
    }

    public boolean isStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state))
            return true;
        return false;
    }

    public void toast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

    }

    public void findAllViewAndSetOnClick() {
        btnNewDir = (Button) view.findViewById(R.id.newDir_btn);
        btnNewDir.setOnClickListener(this);
        btnDelete = (Button) view.findViewById(R.id.delete_btn);
        btnDelete.setOnClickListener(this);
        btnBack = (Button) view.findViewById(R.id.back_btn);
        btnBack.setOnClickListener(this);
        txtPath = (TextView) view.findViewById(R.id.path_text);
        txtPath.setOnClickListener(this);
        edtName = (EditText) view.findViewById(R.id.name_edit);
        edtName.setOnClickListener(this);
    }
}
