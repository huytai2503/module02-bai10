package edu.khtn.bai09_fragment.fragments;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import edu.khtn.bai09_fragment.R;

public class ImgFragment extends Fragment{
    static public String CURRENT_PATH = "";
    LinearLayout imgLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = new LinearLayout(getActivity());
        imgLayout = (LinearLayout) view.getRootView();
        imgLayout.setBackgroundColor(Color.parseColor("#f0f0f0"));
        ImageView imageView = new ImageView(getActivity());
        Drawable drawable = Drawable.createFromPath(CURRENT_PATH);
        imageView.setImageDrawable(drawable);
        imgLayout.addView(imageView);
        return view;
    }
}
